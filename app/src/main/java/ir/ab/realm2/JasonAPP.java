package ir.ab.realm2;

public class JasonAPP {
    //Title
    private String Title;
    //Details
    private String Details;
    //position
    private int Position;

    public String getTitle() {
        return Title;
    }

    public String getDetails() {
        return Details;
    }

    public int getPosition() {
        return Position;
    }

}
