package ir.ab.realm2;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import ir.ab.realm2.Model.Player;
import ir.ab.realm2.Repository.PlayerRepository;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecycleApp adapter;
    private static List<JasonAPP> list;
    private String json;
    public static int i;

    private ProgressBar progressBar, progressBar1;
    private TextView progress_text;
    private String FilePath, Path, link_file, keyMain;
    private File file, directory;
    private static int showSnack = 1;
    private static String status;

    private Player player1, player2;
    PlayerRepository playerRepository;

    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sessionManager = new SessionManager(this);
        keyMain = sessionManager.getKeyMain();
        Log.i("tag", "--------------->" + keyMain);

        recyclerView = findViewById(R.id.aa_recycleApp);
        progressBar = findViewById(R.id.ap_progressbar2);
        progressBar1 = findViewById(R.id.ap_progressbar1);
        progress_text = findViewById(R.id.ap_txtProgress);
        progressBar.setMax(100);

        //<editor-fold desc="قسمت پر کردن ریسایکل">
        JasonAPP_Reader();
        adapter = new RecycleApp(list, this);
        recyclerView.setAdapter(adapter);
        GridLayoutManager llm = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(llm);

        DatabaseCreate();

        DatabaseDownload();


    }

    //ساخت دیتابیس اولیه
    private void DatabaseCreate() {
        //برای اینکه فقط یکبار دیتابیس دانلود شود
        if (keyMain.equals("0")) {
            playerRepository = new PlayerRepository();
            player1 = new Player();
            player2 = new Player();

            player1.setId("A1");
            player1.setName("000");
            player2.setFamily("000");
            player1.setImageNumber("101");
            player1.setLock2(false);
            playerRepository.save(player1);

            player2.setId("A2");
            player2.setName("");
            player2.setFamily("");
            player2.setImageNumber("");
            player2.setLock2(true);
            playerRepository.save(player2);
        }
    }

    //دانلود دیتابیس اصلی
    private void DatabaseDownload() {
        link_file = "https://dl.dropboxusercontent.com/s/6fqbd5qrrmgrjhd/esse.realm?dl=0";

        //گرفتن ادرس ذخیره فایل روت گوشی
        directory = getApplicationContext().getFilesDir();
        //ساخت ادرس مورد نظر
        Path = directory + "/";
        //اضافه کردن نام فایل به ادرس و ساخت ادرس نهایی
        FilePath = Path + getFileNameFromURL(link_file);
        Log.i("tag", FilePath);

        file = new File(FilePath);
        //</editor-fold>

        // اگر فایل موجود بود، باز شود و گرنه دانلود اغاز گردد
        if (file.exists()) {
            if (keyMain.equals("0")) {
                if (file.exists()) {
                    file.delete();
                    Log.i("tag", "موفقیت - فایل ناقص توسط کاربر حذف گردید!!");

                } else {
                    Log.i("tag", "عدم موفقیت - فایلی موجود نبود تا توسط کاربر حذف گردد!!");

                }
                DownloadMethode();
                sessionManager.setKeyMain("1");
            } else {
                Log.i("tag", "موفقیت - فایل موجود بود و نشان داده شده");
                OpenFile();
            }

        } else {
            //اگر اینترنت برقرار بود و گرنه نشان دادن پیغام خطا
            DownloadMethode();

        }
    }

    //متد باز کردن و نشان دادن فایل
    private void OpenFile() {
        progressBar1.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        progress_text.setVisibility(View.GONE);
        showSnack = 0;

    }

    //متد دانلود برای زمانی ک کاربر رفرش میدهد
    private void DownloadMethode() {
        Log.i("tag", "عدم موفقیت - فایل نیست و رفت برای دانلود");
        //اجرای متد دانلود
        new DownloadClass().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, link_file);

        //<editor-fold desc="توقف دانلود بعد از 14.5 ثانیه اگر هنوز تمام نشده باشد">
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (showSnack == 1) {
                    try {
                        Snackbar snackbar = Snackbar
                                .make(findViewById(R.id.ap_consLayout), "اگر فایل دانلود نمی شود، کلیک کنید", 3300)
                                .setAction("دانلود مجدد", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                    }
                                });

                        snackbar.setActionTextColor(getResources().getColor(R.color.red_btn));
                        View sbView = snackbar.getView();
                        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(getResources().getColor(R.color.zard));
                        snackbar.show();

                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 2000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (status.equals("1") | status.equals("2")) {
                    Log.i("tag", status + "عدم موفقیت - زمان دانلود بیش از اندازه طولانی شد");

                    new DownloadClass().cancel(true);

//                    pdfView.setVisibility(View.GONE);
                } else {
                    Log.i("tag", "موفقیت - فایل زودتر از زمان پیشنهادی با موفقیت دانلود شد");

                }
                progressBar1.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                progress_text.setVisibility(View.GONE);

            }
        }, 14500);

        //</editor-fold>
    }

    //گرفتن نام فایل از ادس
    public String getFileNameFromURL(String url) {
        if (url == null) {
            return "";
        }
        try {
            URL resource = new URL(url);
            String host = resource.getHost();
            if (host.length() > 0 && url.endsWith(host)) {
                // handle ...example.com
                return "";
            }
        } catch (MalformedURLException e) {
            return "";
        }

        int startIndex = url.lastIndexOf('/') + 1;
        int length = url.length();

        // find end index for ?
        int lastQMPos = url.lastIndexOf('?');
        if (lastQMPos == -1) {
            lastQMPos = length;
        }

        // find end index for #
        int lastHashPos = url.lastIndexOf('#');
        if (lastHashPos == -1) {
            lastHashPos = length;
        }

        // calculate the end index
        int endIndex = Math.min(lastQMPos, lastHashPos);
        return url.substring(startIndex, endIndex);
    }

    //دانلود کردن فایل در رشته ای جدا
    @SuppressLint("StaticFieldLeak")
    private class DownloadClass extends AsyncTask<String, Integer, File> {
        //اماده سازی دانلود
        @Override
        protected void onPreExecute() {
            Log.i("tag", "موفقیت - متد onPer اجرا شد");

            status = "1";
            super.onPreExecute();

            progressBar.setVisibility(View.VISIBLE);
            progressBar1.setVisibility(View.VISIBLE);
            progress_text.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onCancelled() {
            Log.i("tag", "موفقیت - متد onCancel اجرا شد");

            status = "0";
            try {
                if (file.exists()) {
                    file.delete();
                    Log.i("tag", "موفقیت - فایل ناقص توسط برنامه حذف شد!!");

                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("tag", "عدم موفقیت - دانلود کنسل شد ولی فایلی وجود نداشت که برنامه حذف کند");
            }
        }

        //انجام دانلود فایل در پس زمینه
        @Override
        protected File doInBackground(String... strings) {
            status = "2";
            Log.i("tag", "موفقیت - متد doInBack اجرا شد");

            try {
                Log.i("tag", "موفقیت - فایل در حال ساخت است");

                URL url = new URL(strings[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // getting file length
                int lenghtOfFile = conection.getContentLength();

                //گرفتن اطلاعات از سرور
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                File mydir = new File(Path);
                if (!mydir.exists()) {
                    mydir.mkdirs();
                }
                //ذخیره اطلاعات دریافتی از سرور
                OutputStream output = new FileOutputStream(FilePath);
                long total = 0;
                byte data[] = new byte[1024];
                int count;
                while ((count = input.read(data)) != -1) {

                    total += count;
                    publishProgress((int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("tag", "عدم موفقیت - برنامه قادر به ایجاد و ذخیره سازی فایل نیست");

            }
            if (isCancelled()) {
                if (file.exists()) {
                    file.delete();
                    Log.i("tag", "موفقیت - فایل ناقص در بکگراند حذف شد");
                }
            }

            return null;
        }

        // نشان دادن میزان پیشرفت دانلود
        protected void onProgressUpdate(Integer... progress) {
            progressBar.setProgress(progress[0]);
        }

        //پایان دانلود و ذخیره فایل
        @Override
        protected void onPostExecute(File f) {
            Log.i("tag", "موفقیت - متد onPost اجرا شد");
            Log.i("tag", "موفقیت - فایل به درستی دانلود و نشان داده شد");

            status = "3";
            super.onPostExecute(f);
            file = new File(FilePath);
            if (file.exists()) {
                OpenFile();
            }
        }

    }


    private void JasonAPP_Reader() {
        try {
            InputStream inputStream = getApplicationContext().getAssets().open("app.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            JSONObject obj = new JSONObject(json);
            JSONArray arr = obj.getJSONArray("level1");
            list = new Gson().fromJson(arr.toString(), new TypeToken<List<JasonAPP>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
