package ir.ab.realm2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {

    // Logcat Tag
    private static String TAG = SessionManager.class.getSimpleName();
    private SharedPreferences pref;
    private Editor editor;
    private Context _context;
    //Shared Mode
    private int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "RRR";
    //شماره ایتم مورد نظر
    private static final String KEY_MAIN = "main";


    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this._context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    public void setKeyMain(String keyMain) {
        editor.putString(KEY_MAIN, keyMain);
        editor.commit();
    }

    public String getKeyMain() {
        return pref.getString(KEY_MAIN, "0");
    }

}