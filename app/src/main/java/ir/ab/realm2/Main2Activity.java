package ir.ab.realm2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import ir.ab.realm2.Repository.PlayerRepository;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class Main2Activity extends AppCompatActivity {
    PlayerRepository playerRepository ;
    String url;
    ImageView imageView;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        TextView name = findViewById(R.id.txt_name);
        TextView family = findViewById(R.id.txt_family);
        imageView = findViewById(R.id.img);

        id = "A" + String.valueOf(MainActivity.i);
        Log.i("realm", id);

        playerRepository = new PlayerRepository();

        Log.i("realm", " ----------->" + playerRepository.findByID(id).toString());

        name.setText(playerRepository.findByID(id).getName());
        family.setText(playerRepository.findByID(id).getFamily());

        /*ImageDataBase();*/
    }

    /*private void ImageDataBase() {
        String drawable = playerRepository.findByID(id).getImageNumber();

        drawable = "image_" + drawable + ".jpg";

        Log.i("realm", drawable);


        url = "file:///android_asset/" + drawable;
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(this)
                .load(url)
                .apply(options)
                .transition(withCrossFade())
                .into(imageView);
    }*/

    @Override
    public void onBackPressed() {
        playerRepository.close();
        super.onBackPressed();
    }
}
