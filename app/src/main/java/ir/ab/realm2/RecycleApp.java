package ir.ab.realm2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RecycleApp extends RecyclerView.Adapter<RecycleApp.MyViewHolder> {
    private Context context;
    private static List<JasonAPP> list;


    public RecycleApp(List<JasonAPP> list, Context context) {
        this.context = context;
        RecycleApp.list = list;


    }

    @NonNull
    @Override
    public RecycleApp.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        View v = li.inflate(R.layout.card_recycle_app, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecycleApp.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.txtTitle.setText(list.get(position).getTitle());
        holder.txtDetails.setText(list.get(position).getDetails());
        holder.imgIcon.setImageResource(R.drawable.ic_launcher_background);
        holder.itemView.setOnClickListener(v -> {
            //<editor-fold desc="Download App">
            Intent intent = new Intent(context, Main2Activity.class);
            MainActivity.i = position + 1;
            context.startActivity(intent);
            //</editor-fold>
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle, txtDetails;
        ImageView imgIcon;

        MyViewHolder(View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.cr_txtTitle);
            txtDetails = itemView.findViewById(R.id.cr_txtDetails);
            imgIcon = itemView.findViewById(R.id.cr_imgIcon);
        }
    }


}