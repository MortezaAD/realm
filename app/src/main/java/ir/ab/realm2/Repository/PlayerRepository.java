package ir.ab.realm2.Repository;

import io.realm.Realm;
import ir.ab.realm2.Model.Player;

public class PlayerRepository {
    private Realm realm;

    public PlayerRepository() {
        realm = Realm.getDefaultInstance();
    }

    public void save(final Player player) {
        realm.executeTransaction(realm -> realm.copyToRealmOrUpdate(player));
    }

    public Player findByID(String id) {
        Player player = realm.where(Player.class).equalTo("id", id).findFirst();

        return player;
    }

    public void close() {
        realm.close();
    }

}
